# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# ==============================Aliases==========================================
alias vi=vim
alias ll='ls -lah --color=auto'
alias cal='cal -m'
alias today='today.py -p ~/Dropbox/DATA/'
alias yesterday='today -d -1'
alias vi=vim
alias ls='ls --color="auto" -CFB' ll='ls -lh' la='ls -A' lsd='ls -d' l='ls'
alias mkdir='mkdir -p' 
alias du='du -h' df='df -h'
alias tree='tree -C'
alias free='free -h'
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
alias gru="git remote update"
alias gst="git status"

# ===============================ENV_VARS========================================
export HOMEBANK_DIR="/home/ahub/Dropbox/COUPLE/Classeur maison/Finance/homebank
"
export PATH="${PATH}:~/bin"
export EDITOR="vim"

# ===============================Prompt========================================
# Colors :
RST="$(tput sgr0)"  #Reset
R="$(tput setaf 1)" #Red
G="$(tput setaf 2)" #Green
B="$(tput setaf 4)" #Blue
Y="$(tput setaf 3)" #Yellow

# Time last command
function timer_start {
  timer=${timer:-$SECONDS}
}

function timer_stop {
  timer_show=$(($SECONDS - $timer))
  unset timer
}

trap 'timer_start' DEBUG
PROMPT_COMMAND=timer_stop
# /Time last command

# The \[ \] are only special when you assign PS1, if you print them inside a function that runs when the prompt is displayed it doesn't work.
PS1='\n[ \[${B}\]\w\[${RST}\] ] [ \[${G}\]\T\[${RST}\] | \[${B}\]\[${timer_show}\]s\[${RST}\] ]\n[ \[${Y}\]\u\[${RST}\] | \[${G}\]\h\[${RST}\] ]$ '

# ===============================FUNCTIONS========================================
#netinfo - shows network information for your system
netinfo ()
{
echo "--------------- Network Information ---------------"
echo "Wireless"
ip -4 addr show wlp6s0 | awk /'inet/ {print $2}'
echo "Ethernet"
ip -4 addr show enp7s0 | awk /'inet/ {print $2}'
echo "Public"
curl ipecho.net/plain; echo
echo "---------------------------------------------------"
}

# ===============================HISTORY========================================
export HISTCONTROL=ignoredups:erasedups  # no duplicate entries
export HISTSIZE=1000000
export HISTFILESIZE=1000000
export HISTTIMEFORMAT='%F %T '
shopt -s histappend                      # append to history, don't overwrite it

# Save and reload the history after each command finishes
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

# ===============================Startup=======================================
fish && exit
