# ==============================Tweaks===========================================
set fish_greeting #Disables greeting

# ===============================ENV_VARS========================================
set -x PREDICTION_DB "/home/ahub/Dropbox/DATA/predict.pickle"
set -x HOMEBANK_DIR "/home/ahub/Dropbox/COUPLE/Classeur maison/Finance/homebank"
set -x TODAY_DB "/home/ahub/Dropbox/DATA/today.json"
set -x CAPLOG_DB "/home/ahub/Dropbox/DATA/caplog.pickle"
set -x EDITOR "vim"
set -x PATH $PATH ~/bin/
#set -x PATH $PATH ~/.local/bin/
set -x EDITOR vim
#set -x LANG en_US.UTF-8
# Android sdk
set -x  ANDROID_SDK /opt/android-sdk
#set -x  ANDROID_NDK /opt/android-ndk
set -x  ANDROID_SWT /usr/share/java
set -x  ANDROID_HOME $ANDROID_SDK
set -x  JAVA_HOME /usr/lib/jvm/java-8-openjdk
set -x  PATH $PATH $ANDROID_SDK/tools $ANDROID_NDK
set -x  PATH $PATH $ANDROID_SDK/platform-tools

# ==============================Aliases==========================================
alias ll='ls -lah --color=auto'
alias today='today.py'
alias dataself='dataself.py'
alias yesterday='today -d=1'
alias vi=vim
alias vim=vim
alias ls='ls --color="auto" -CFB' 
alias ll='ls -lh' 
alias la='ls -A' 
alias lsd='ls -d' 
alias l='ls'
alias mkdir='mkdir -p' 
alias du='du -h' 
alias df='df -h'
#alias tree='tree -C'
alias free='free -h'
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
alias gru="git remote update"
alias gst="git status"
alias journalctl_live="journalctl -xef --no-hostname -o short-iso"
alias armageddon='telnet armageddon.org 4050'
alias ss='cmatrix -as; clear'

# ===============================FUNCTIONS========================================
#netinfo - shows network information for your system
function netinfo 
    echo "--------------- Network Information ---------------"
    echo -n "Wireless : "
    ip -4 addr show wlp58s0 | awk /'inet/ {print $2}'
    echo
    echo -n "Ethernet : "
    ip -4 addr show enp7s0 | awk /'inet/ {print $2}'
    echo
    echo -n "Public : "
    echo (curl -s ipecho.net/plain)
    echo
    echo "---------------------------------------------------"
end

# ===============================Startup=======================================
#today
predict
cat todo

