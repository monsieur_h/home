function fish_prompt
    and set retc green; or set retc red
	tty|grep -q tty; and set tty tty; or set tty pts
# Vars
    set -l u (whoami)
    set -l h (hostname)
    set -l p (prompt_pwd) #Replace with `pwd` maybe ?
    set -l t (date | grep -oE "[0-9:]{8}")
    set -l l (uptime | cut -d "," -f 4 | grep -Eo "[0-9\\.]+")
    if [ $h = 'nostromo' ]
        set -g h_color red
    else
        set -g h_color yellow
    end
    if test $USER = root
        set uc red
		set symbol "#"
    else
        set uc cyan
		set symbol "\$"
    end
	if test -z $CMD_DURATION 
	   set duration 0
	else
	   set duration $CMD_DURATION
	end

#Fist line
    set_color $retc
    if [ $tty = tty ]
        echo -n .-
    else
        echo -n '┬─'
    end

    set_color normal #pwd 
    echo -n "["
    set_color cyan
    echo -n $p
    set_color normal
    echo -n "]"

    set_color $retc #separator
    if [ $tty = tty ]
        echo -n '-'
    else
        echo -n '─'
    end

    set_color normal
    echo -n "["
    set_color green
    echo -n $t
    set_color normal
    echo -n "]"

    set_color $retc #separator
    if [ $tty = tty ]
        echo -n '-'
    else
        echo -n '─'
    end

	set_color normal
    echo -n "["
    set_color cyan
    echo -n (math $duration/1000)"s"
    set_color normal
    echo -n "]"

    set_color $retc #separator
    if [ $tty = tty ]
        echo -n '-'
    else
        echo -n '─'
    end
    
    set_color normal
    echo -n "["
    set_color cyan
    echo -n $l
    set_color normal
    echo -n "]"


	echo # End of first line
#Jobs lines
for job in (jobs)
        set_color $retc
		echo -n '| '
        set_color cyan
        echo $job
end

#Second line
    set_color $retc
    if [ $tty = tty ]
        echo -n "'->"
    else
        echo -n '╰─>'
    end
    set_color normal
    echo -n "["
	set_color $uc
    echo -n $u
    set_color normal
    echo -n "@"
    set_color $h_color
    echo -n $h
    set_color normal
    echo -n "]"

    set_color $retc #separator
    if [ $tty = tty ]
        echo -n '-'
    else
        echo -n '─'
    end

	set_color $uc
	echo -n "$symbol "
end
